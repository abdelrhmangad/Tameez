//! About Page
// show nav bar on clicking on menu icon
let menuIcon = document.getElementById("menuIcon");
let navbarList = document.getElementById("navbarList");
navbarList.classList.toggle("hide");
function showNavBar() {
  navbarList.classList.toggle("hide");
}

//! Contact Us Page
//Form validation
const contactForm = document.contactForm;
const fullName = document.contactForm.fullName;
const Email = document.contactForm.Email;
const Address = document.contactForm.Address;
const message = document.getElementById("message");

function validate(event) {
  event.preventDefault();
  if (fullName.value == "") {
    fullName.classList.add("error");
    fullName.focus();
    return false;
  } else {
    fullName.classList.remove("error");
  }
  if (Email.value == "") {
    Email.classList.add("error");
    Email.focus();
    return false;
  } else {
    Email.classList.remove("error");
  }
  if (Address.value == "") {
    Address.classList.add("error");
    Address.focus();
    return false;
  } else {
    Address.classList.remove("error");
  }
  if (message.value == "") {
    message.classList.add("error");
    message.focus();
    return false;
  } else {
    message.classList.remove("error");
  }
  return true;
}
